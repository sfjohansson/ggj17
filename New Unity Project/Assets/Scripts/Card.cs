﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;

public enum WaveAction {
	HIGHER,
	LOWER,
	WIDER,
	THINNER,
};

public class Card : MonoBehaviour {
	public bool selectable = true;
	public int cardNumber;
	public TextMeshProUGUI cardText;
	public Image cardImage;
	public WaveAction action;
	public int actionAmount = 1;
	public Image actionImage;

	public Sprite[] actionImages;

	void Start () {
		UpdateText ();
	}
	
	void Update () {
	}

	void UpdateText()
	{
		cardText.text = string.Format ("{0} {1}", actionAmount, action);
	}

	public void Selected()
	{
		selectable = false;
		 
		cardImage.DOFade(0, 0.2f).OnComplete (()=>{
			cardImage.DOFade(1, 0.2f).SetDelay(0.5f).OnComplete(()=>{
				action = (WaveAction)Random.Range (0, 4);
				actionAmount = Random.Range (1, 4);		

				UpdateText();
				selectable = true;
			});
		});
	}
}
