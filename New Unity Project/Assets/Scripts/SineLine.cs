﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SineLine : MonoBehaviour
{
    [Header("Anim Attributes")]
    public float waveSpeed = 1;

    [Header("Rendering Attributes")]
    public int samples=48;
    public float drawDistance =50f;

    [Header("Wave Attribtues")]
    public float height=1f;
    public int waveCount =1;
    public float waveLength=1;

    LineRenderer m_lineRenderer;

    Vector3[] positions;

// Use this for initialization
    void Awake()
    {
        m_lineRenderer=GetComponent<LineRenderer>();
    }



    void Update()
    {
        positions=CalcualtePoints();

        m_lineRenderer.numPositions= waveCount *samples;

        m_lineRenderer.SetPositions(positions);
    }



    Vector3[] CalcualtePoints()
    {

        int totalNumberOfPoints= waveCount *samples;

        Vector3[] samplePoints= new Vector3[totalNumberOfPoints];

        float delta= drawDistance /totalNumberOfPoints;

        for (int i = 0; i < totalNumberOfPoints; i++)
        {
            samplePoints[i]=new Vector3(i*delta, height*Mathf.Sin(waveSpeed *Time.realtimeSinceStartup+i*delta/waveLength));
        }

        return samplePoints;
    }
}
