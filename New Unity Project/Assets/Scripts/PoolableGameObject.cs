﻿using UnityEngine;
using System.Collections;
using UnityEngine.Serialization;

public class PoolableGameObject : MonoBehaviour 
{
    [Header("PoolableGameObject Settings")]

    [FormerlySerializedAs("poolable")]
    public bool poolingEnabled;

}
