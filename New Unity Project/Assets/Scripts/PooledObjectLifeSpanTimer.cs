using UnityEngine;
using System.Collections;

public class PooledObjectLifeSpanTimer : MonoBehaviour 
{

	public float lifeSpan;

	// Use this for initialization
	void OnEnable() 
	{
		StartCoroutine(DelayedDestroyPooled());
	}

	IEnumerator DelayedDestroyPooled()
	{
		yield return new WaitForSeconds(lifeSpan);

		ObjectPoolManager.DestroyPooled (this.gameObject);
	}
}
