﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Animal : MonoBehaviour {
	float floatDistance = 1.2f;
	float floatTime = 1f;

	void Start () {
		transform.DOLocalMoveY(floatDistance, floatTime).SetEase(Ease.InOutBack).SetLoops(-1, LoopType.Yoyo);
	}

	void Update () {
		
	}
}
