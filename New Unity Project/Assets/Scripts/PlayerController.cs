﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    VariableSineLine m_playerLine;

    public int lengthSteps;
    public int hightSteps;

    float m_currentHeight;
    float m_currentLength;

    float m_heightStepSize;
    float m_lengthStepSize;

    public void Initialize(VariableSineLine line)
    {
        if (line!=null)
        {
            m_playerLine=line;

            m_currentHeight=m_playerLine.height;
            m_currentLength=m_playerLine.waveLength;

            m_lengthStepSize =m_currentLength/lengthSteps;
            m_heightStepSize=m_currentHeight/hightSteps;

            RandomStepsHeight();
            RandomStepsLength();

            line.Initialize(m_currentHeight, m_currentLength);

        }
        else Debug.LogError("Initialize Controller Failed");
    }

    public void RandomStepsHeight()
    {
        m_currentHeight=m_currentHeight+ (float) Random.Range(-5,5)*m_heightStepSize;
    }

    public void RandomStepsLength()
    {
        m_currentLength+= Random.Range(-5,5)*m_lengthStepSize;
    }

	public void IncreaseLength(int amount = 1)
    {
        #if UNITY_DEBUG
        Debug.Log("IncreaseLength");
        #endif
		m_currentLength+= (m_lengthStepSize * amount);
    }

	public void DecreaseLength(int amount = 1)
    {
        #if UNITY_DEBUG
        Debug.Log("DeIncreaseLength");
        #endif

		m_currentLength-= (m_lengthStepSize * amount);
    }

	public void IncreaseHeight(int amount = 1)
    {
        #if UNITY_DEBUG
        Debug.Log("IncreaseHeight");
        #endif
		m_currentHeight+= (m_heightStepSize * amount);
    }

	public void DecreaseHeight(int amount = 1)
    {
        #if UNITY_DEBUG
        Debug.Log("DeIncreaseHeight");
        #endif
		m_currentHeight-=(m_heightStepSize * amount);
    }

    void Update()
    {
        if (m_playerLine!=null)
            m_playerLine.SetLineControlVariables(m_currentHeight,m_currentLength);
    }

    void OnDisable()
    {
        m_playerLine=null;
    }
}
