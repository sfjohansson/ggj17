﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[System.Serializable]
public class LevelDefinition
{
    public string levelName;
    public float threshold;
    public float waveLength;
    public float waveSpeed;
    public float waveHight;
}



public class LevelManager : MonoBehaviour
{
	public Transform canvasParent;

	public static LevelManager instance;

    public LevelState levelState;

    public enum LevelState
    {
        Start,
        Warmup,
        InProgress,
        Evaluation,
        End
    }

    public float threshold=0.1f;

    public VariableSineLine target;
    public VariableSineLine playerLinePrefab;
    public PlayerController playerControllerPrefab;

    public List<PlayerController> m_playerControllers= new List<PlayerController>();
    List<VariableSineLine> m_playerLines=new List<VariableSineLine>();

    public GameObject scoringVFXPrefab;

    public Color[] playerColors;


    void Awake()
    {
        instance=this;
    }

    void OnEnable()
    {
//        for (int i = 0; i < m_playerLines.Length; i++)
//        {
//            m_playerLines[i].gameObject.SetActive(false);
//        }
    }

    public void StartNewLevel(int numberOfPlayers, LevelDefinition levelDef)
    {
        Debug.Log("StartNewLevel: "+levelDef.levelName);

        levelState=LevelState.Start;

        target.SetLineControlVariables(levelDef.waveHight, levelDef.waveLength);
        target.gameObject.SetActive(true);

        BuildPlayers(numberOfPlayers);

        StartCoroutine(WarmUP());
    }


    IEnumerator WarmUP()
    {

        levelState=LevelState.Warmup;

        yield return new WaitForSeconds(1f);

        levelState=LevelState.InProgress;
    }

    void BuildPlayers(int numberOfPLayers)
    {
        Debug.Log("BuildPlayers: "+numberOfPLayers);

        m_playerLines.Clear();
        m_playerControllers.Clear();

        for (int i = 0; i < numberOfPLayers; i++)
        {
            VariableSineLine newPlayerLine=GameObject.Instantiate<VariableSineLine>(playerLinePrefab);

			// Move to canvas
			newPlayerLine.transform.SetParent(canvasParent, true);

            newPlayerLine.SetColor(playerColors[i]);

            m_playerLines.Add(newPlayerLine);

            newPlayerLine.gameObject.SetActive(true);

            PlayerController newPlayerController=GameObject.Instantiate(playerControllerPrefab).GetComponent<PlayerController>();

            m_playerControllers.Add(newPlayerController);

            newPlayerController.Initialize(newPlayerLine);
        }
    }

	void Update ()
    {
        if (levelState==LevelState.InProgress) UpdateInProgressState();
	}

    void UpdateInProgressState()
    {
        if (m_playerLines.Count!=0)
        {
            int result=-1;

            for (int i = 0; i < m_playerLines.Count; i++)
            {
                VariableSineLine playerLine=m_playerLines[i];

                if (CompareLines(playerLine, target)==true)
                {
                    Debug.Log("result: "+i);
                    result=i;
                    break;
                }
            }

            if (result!=-1)
            {
                Debug.Log("result not minus one");
                ScoringVFXForLine(m_playerLines[result]);
                LevelCompleted(result);
            }
        }

    }

    void LevelCompleted(int winner)
    {
        Debug.Log("LevelCompleted: "+winner);

        levelState=LevelState.End;
        GameManager.instance.ReportWinner(winner);

        DestroyLevel();
    }

    public void DestroyLevel()
    {
        Debug.Log("DestroyLevel");

        for (int i = 0; i < m_playerLines.Count; i++)
        {
            Destroy(m_playerLines[i].gameObject);
        }

        for (int i = 0; i < m_playerLines.Count; i++)
        {
            Destroy(m_playerControllers[i].gameObject);
        }

        m_playerLines.Clear();
        m_playerControllers.Clear();

        levelState=LevelState.Start;
    }

    bool CompareLines(VariableSineLine line, VariableSineLine referenceLine)
    {
        //Debug.Log("CompareLines");

        bool allPointsMatch=true;

		if (referenceLine.samples==line.samples)
        {
            Vector3[] linePoints=line.GetCurrentControlPoints();
			Vector3[] referencePoints= referenceLine.GetCurrentControlPoints();
            int numberOfPoints= referencePoints.Length;

            for (int i = 0; i <numberOfPoints; i++)
			{
                if(ComparePoints(linePoints[i], referencePoints[i])==false)
                {
                    allPointsMatch=false;
                    break;
                }
			}

            //allPointsMatch=false;
        }

        if (allPointsMatch) Debug.Log("CompareLines allpoints Match");

        return allPointsMatch;
    }

    bool ComparePoints(Vector3 point1, Vector3 point2)
    {
        float delta=Vector3.Distance(point1, point2);

        if (delta<threshold) return true;
        else return false;
    }

    void ScoringVFXForLine(VariableSineLine playeLine)
    {
        Vector3[] linePoints=playeLine.GetCurrentControlPoints();

        for (int i = 0; i < linePoints.Length; i++)
        {
            if (i% 5 != 0)ObjectPoolManager.CreatePooled(scoringVFXPrefab, linePoints[i],Quaternion.identity);
        }
    }


}
