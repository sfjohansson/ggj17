﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveGenerator : MonoBehaviour
{
	public GameObject cursor;

	public float amplitude;

    public float waveLength;

    float m_value;

	// Update is called once per frame
	void Update ()
    {
        m_value+=Time.deltaTime;

        cursor.transform.position=Vector3.zero+Vector3.up*amplitude*Mathf.Sin(m_value/waveLength);
	}
}
