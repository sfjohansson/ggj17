﻿using UnityEngine;

//There is chance this is working as reset ios only called by teh inspector..so it might be ok in game
public class PrefabReference : MonoBehaviour 
{
    public GameObject prefabRef;

    void Reset()
    {
        if (prefabRef==null)
        {
            prefabRef=this.gameObject;
        }
    }



//    unfortunately there is not any other way. you should create a public GameObject variable
//for each of those GameObjects and set that variable to the prefab in inspector. you can not find or
//get reference to prefabs by any means at runtime. (this is a big limitation that should be removed).
//    keep in mind that when yousay this.gameObject it will give you
//a reference to current instance so after distruction it will becom null.

}
