﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerStats
{
    public List<int> levelsWon= new List<int>();

    public int NumberOfLevelsWon
    {
        get { return levelsWon.Count; }
    }
}

public class GameManager : MonoBehaviour
{
    public GameState gameState;

    public enum GameState
    {
        Idle,
        Menu,
        Starting,
        GameInProgress,
        ProgressingToNextLevel,
        AllLevelsCompleted,
        GameOver
    }

    public static GameManager instance;

    public int numberOfPlayers;

    public List<PlayerStats> playerStats;

    public LevelDefinition[] levels;

    public int currentLevel;

    public int winnerLastRound=-1;

    void Awake()
    {
        instance=this;
    }

	void OnEnable()
    {
        gameState=GameState.Menu;
    }

    public void StartGame()
    {
        StartGame(numberOfPlayers);
    }

	public void StartGame(int players)
    {
        numberOfPlayers=players;

        currentLevel=0;

        playerStats.Clear();

        for (int i = 0; i < numberOfPlayers; i++)
        {
            playerStats.Add(new PlayerStats());
        }

        StartCoroutine(_StartingGame());
	}

    IEnumerator _StartingGame()
    {
        gameState=GameState.Starting;

        while (LevelManager.instance==null) yield return null;

        LevelManager.instance.StartNewLevel(numberOfPlayers,levels[currentLevel]);

        gameState=GameState.GameInProgress;
    }

    public string GetCurrentLevelName()
    {
        return levels[currentLevel].levelName;
    }

    void NextLevel()
    {
        StartCoroutine(_LoadNextLevel());
    }

    IEnumerator _LoadNextLevel()
    {
        gameState=GameState.ProgressingToNextLevel;

        while (LevelManager.instance.levelState!=LevelManager.LevelState.Start) yield return null;

        yield return new WaitForSeconds(1f);

        currentLevel++;

        LevelDefinition newLevelDef=levels[currentLevel];

        LevelManager.instance.StartNewLevel(numberOfPlayers, newLevelDef);

        gameState=GameState.GameInProgress;
    }

    public void ReportWinner(int winnerNumber)
    {
        Debug.Log("ReportWinner: "+winnerNumber);

        if (playerStats.Count>winnerNumber && playerStats[winnerNumber]!=null)
        {
            playerStats[winnerNumber].levelsWon.Add(currentLevel);
        }

        if (IsLastLevel(currentLevel)==false) NextLevel();
        else AllLevelsCompleted();
    }

    void AllLevelsCompleted()
    {
		Debug.Log("AllLevelsCompleted");

        winnerLastRound=FindWinner();

        gameState=GameState.AllLevelsCompleted;
    }

    bool IsLastLevel(int levelNumber)
    {
        if (levelNumber==levels.Length-1) return true;
        else return false;
    }

    int FindWinner()
    {
        int highestRankedPlayerIndex=0;
        int highestScore=0;

        if (numberOfPlayers!=1)
        {
            for (int i = 0; i < numberOfPlayers; i++)
            {
                if (playerStats[i].levelsWon.Count>highestScore) highestRankedPlayerIndex=i;
                else if (playerStats[i].levelsWon.Count==highestScore) highestRankedPlayerIndex=-2;
            }
        }

        return 0;
    }

    public void QuitToMenu()
    {
        LevelManager.instance.DestroyLevel();

        gameState=GameState.Menu;

    }
}
