﻿using UnityEngine;



public class VariableSineLine : MonoBehaviour
{
    [Header("Controll Attributes")]
    public bool aiControlled;


    [Header("Anim Attributes")]
    public float waveSpeed = 1;
    public float lerp=0.01f;

    [Header("Rendering Attributes")]
    public int samples=48;
    public float drawDistance =50f;

    [Header("Wave Attribtues")]
    public float height=1f;
    public int waveCount =1;
    public float waveLength=1;

    LineRenderer m_lineRenderer;

    Vector3[] positions;

	// Use this for initialization
	void Awake()
    {
		m_lineRenderer=GetComponent<LineRenderer>();
	}

    void OnEnable()
    {
        if (aiControlled)
        {
            Initialize(height, waveLength);
        }
    }

    float m_currentHeight;
    float m_currentWaveLength;

    bool m_initialized=false;

    void Update()
    {
        if (m_initialized)
        {
            if (aiControlled) UpdateAILineController();

            positions=CalcualtePoints(m_currentHeight, m_currentWaveLength);

            m_lineRenderer.numPositions= waveCount *samples;

            m_lineRenderer.SetPositions(positions);
        }

    }

    public void Initialize(float waveHeight, float waveLength)
    {
        m_initialized=true;
        m_currentHeight=waveHeight;
        m_currentWaveLength=waveLength;
    }
    public void SetLineControlVariables(float waveHeight, float waveLength)
    {
        m_currentHeight=waveHeight;
        m_currentWaveLength=waveLength;
    }

    public void SetColor(Color newColor)
    {
        m_lineRenderer.startColor=newColor;
    }

    public void UpdateAILineController()
    {
//        m_currentHeight=0.5f*height+0.5f*height*Mathf.Sin(waveSpeed *Time.realtimeSinceStartup);//Mathf.Lerp(m_currentHeight, 0f, waveLength/totalNumberOfPoints);
//
//        m_currentWaveLength=0.7f*waveLength+0.3f*waveLength*Mathf.Sin(waveSpeed *Time.realtimeSinceStartup);
    }

    Vector3[] m_currentPoints=new Vector3[1];

	Vector3[] CalcualtePoints(float waveHeight, float waveLength)
    {
        int totalNumberOfPoints= waveCount *samples;

        m_currentPoints= new Vector3[totalNumberOfPoints];

        float delta= drawDistance /totalNumberOfPoints;

		for (int i = 0; i < totalNumberOfPoints; i++)
		{
            m_currentPoints[i]=new Vector3(i*delta-drawDistance*0.5f, m_currentHeight*Mathf.Sin(waveSpeed *Time.realtimeSinceStartup+i*delta/m_currentWaveLength));
		}

        return m_currentPoints;
    }
    
    public Vector3[] GetCurrentControlPoints()
    {

        return  m_currentPoints;
    }

    void OnDisable()
    {
        m_initialized=false;
    }
}
