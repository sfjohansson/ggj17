﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using UnityEngine.UI;

public class PlayerActions : MonoBehaviour {
	Player player;
	public int playerId;
	bool buttonA;
	bool buttonB;
	bool buttonX;
	bool buttonY;

	Card card1;
	Card card2;
	Card card3;
	Card card4;

	public Transform panelActions;

//	LevelManager.instance. [playerId].
	PlayerController playerController;

	void Awake()
	{
		card1 = panelActions.GetChild (0).GetComponent<Card> ();
		card2 = panelActions.GetChild (1).GetComponent<Card> ();
		card3 = panelActions.GetChild (2).GetComponent<Card> ();
		card4 = panelActions.GetChild (3).GetComponent<Card> ();
	}

	void Start () {
		StartCoroutine (WaitingForGame ());
	}

	IEnumerator WaitingForGame()
	{
		while (GameManager.instance.gameState != GameManager.GameState.GameInProgress)
			yield return null;

		player = ReInput.players.GetPlayer (playerId);

		playerController = LevelManager.instance.m_playerControllers [playerId];
	}
	
	void Update () {
		buttonA = player.GetButtonDown ("A");
		buttonB = player.GetButtonDown ("B");
		buttonX = player.GetButtonDown ("X");
		buttonY = player.GetButtonDown ("Y");

		if (buttonA) {
			if (card1.selectable) {
				switch (card1.action) {

				case WaveAction.HIGHER:
					{
						playerController.IncreaseHeight(card1.actionAmount);
					}
					break;
				case WaveAction.LOWER:
					{
						playerController.DecreaseHeight(card1.actionAmount);
					}
					break;
				case WaveAction.THINNER:
					{
						playerController.DecreaseLength (card1.actionAmount);
					}
					break;
				case WaveAction.WIDER:
					{
						playerController.IncreaseLength (card1.actionAmount);
					}
					break;
				}
					
				card1.Selected ();
			}
		}

		if (buttonB) {
			if (card2.selectable) {
				//				if (card1.selectable) {
				switch (card2.action) {

				case WaveAction.HIGHER:
					{
						playerController.IncreaseHeight(card2.actionAmount);
					}
					break;
				case WaveAction.LOWER:
					{
						playerController.DecreaseHeight(card2.actionAmount);
					}
					break;
				case WaveAction.THINNER:
					{
						playerController.DecreaseLength (card2.actionAmount);
					}
					break;
				case WaveAction.WIDER:
					{
						playerController.IncreaseLength (card2.actionAmount);
					}
					break;
				}

				card2.Selected ();
			}
		}

		if (buttonX) {
			if (card3.selectable) {
				//				if (card1.selectable) {
				switch (card3.action) {

				case WaveAction.HIGHER:
					{
						playerController.IncreaseHeight(card3.actionAmount);
					}
					break;
				case WaveAction.LOWER:
					{
						playerController.DecreaseHeight(card3.actionAmount);
					}
					break;
				case WaveAction.THINNER:
					{
						playerController.DecreaseLength (card3.actionAmount);
					}
					break;
				case WaveAction.WIDER:
					{
						playerController.IncreaseLength (card3.actionAmount);
					}
					break;
				}

				card3.Selected ();
			}
		}

		if (buttonY) {
			if (card4.selectable) {
				switch (card4.action) {

				case WaveAction.HIGHER:
					{
						playerController.IncreaseHeight(card4.actionAmount);
					}
					break;
				case WaveAction.LOWER:
					{
						playerController.DecreaseHeight(card4.actionAmount);
					}
					break;
				case WaveAction.THINNER:
					{
						playerController.DecreaseLength (card4.actionAmount);
					}
					break;
				case WaveAction.WIDER:
					{
						playerController.IncreaseLength (card4.actionAmount);
					}
					break;
				}

				card4.Selected ();
			}
		}
	}
}
