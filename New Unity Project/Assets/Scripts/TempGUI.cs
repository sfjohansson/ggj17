﻿using UnityEngine;

public class TempGUI : MonoBehaviour
{
	public LevelManager levelManager;

    PlayerController playerController;

    void OnGUI()
    {
        switch (GameManager.instance.gameState)
        {
            case GameManager.GameState.Menu: DrawMenu(); break;
            case GameManager.GameState.GameInProgress: DrawIngameGUI(); break;
            case GameManager.GameState.AllLevelsCompleted: AllLevelsCompleted(); break;
        }

    }

    void DrawMenu()
    {
        if (GUILayout.Button("Start 1 Player Game")) GameManager.instance.StartGame(1);
        if (GUILayout.Button("Start 2 Player Game")) GameManager.instance.StartGame(2);
    }

    void AllLevelsCompleted()
    {
        int winnerOrDraw=GameManager.instance.winnerLastRound;

        string textToDisplay;

        if (winnerOrDraw==-2) textToDisplay="Draw";
        else textToDisplay=winnerOrDraw.ToString();

        GUILayout.Label("Game Completed Winner Is: "+textToDisplay);

        if (GUILayout.Button("Play Again ")) GameManager.instance.StartGame();
    }

    void DrawIngameGUI()
    {
        GUILayout.Label("Animal: "+GameManager.instance.GetCurrentLevelName() );


        //Hack to grab controller for oneplayer

        if (LevelManager.instance!=null && LevelManager.instance.levelState==LevelManager.LevelState.InProgress)
        {
            if (LevelManager.instance.m_playerControllers.Count>0 && LevelManager.instance.m_playerControllers[0]!=null)
                playerController=LevelManager.instance.m_playerControllers[0];
        }
        else playerController=null;

        if (playerController!=null & LevelManager.instance.levelState==LevelManager.LevelState.InProgress)
        {
            if (GUILayout.Button("  Increase Height  ")) playerController.IncreaseHeight();
            if (GUILayout.Button(" Decrease Height ")) playerController.DecreaseHeight();

            if (GUILayout.Button("  Increase Length  ")) playerController.IncreaseLength();
            if (GUILayout.Button(" Derease Length ")) playerController.DecreaseLength();
            if (GUILayout.Button("Quit To Main")) GameManager.instance.QuitToMenu();
        }
    }


}
